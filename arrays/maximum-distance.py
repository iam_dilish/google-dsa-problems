def maxDiff(A, n):
    if n == 0:
        return 0
    
    # Create and fill LMin array
    LMin = [0] * n
    LMin[0] = A[0]
    for i in range(1, n):
        LMin[i] = min(A[i], LMin[i - 1])
    
    # Create and fill RMax array
    RMax = [0] * n
    RMax[n - 1] = A[n - 1]
    for j in range(n - 2, -1, -1):
        RMax[j] = max(A[j], RMax[j + 1])
    
    # Traverse LMin and RMax to find maximum j - i
    i, j = 0, 0
    max_diff = -1
    while i < n and j < n:
        if LMin[i] <= RMax[j]:
            max_diff = max(max_diff, j - i)
            j += 1
        else:
            i += 1
    
    return max_diff

# Example usage:
A = [3, 5, 4, 1, 4, 7, 9]
N = len(A)
print(maxDiff(A, N))  # Output: 2
