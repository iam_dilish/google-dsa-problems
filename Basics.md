
# Time Complexity 
- It describes the amount of computational time an algorithm takes to complete as a function of the size of the input.
#### O(1): Constant time - Independent of input size.
#### O(N): Linear time - Grows directly with input size.
#### O(N^2): Quadratic time - Grows with the square of the input size.
#### O(log N): Logarithmic time - Grows slower, doubling the input size adds a constant time.
#### O(N log N): Linearithmic time - Common in efficient sorting.
#### O(2^N): Exponential time - Grows very fast, doubling the input size doubles the time.
#### O(N!): Factorial time - Extremely fast growth, adding one input significantly increases time.